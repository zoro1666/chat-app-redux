import Firebase from 'firebase';

class Backend {
    uid = '';
    messagesRef = null;

    //initialize Firebase Backend
    constructor() {
        Firebase.initializeApp({
            apiKey: "AIzaSyBYQWx0ZDwYYdZhSgeJSIoDjzsdB-sRsWA",
            authDomain: "chats-bcdcd.firebaseapp.com",
            databaseURL: "https://chats-bcdcd.firebaseio.com",
            storageBucket: "chats-bcdcd.appspot.com",
            messagingSenderId: "509081940114"
        });
        Firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setUid(user.uid);
            } else {
                Firebase.auth().signInAnonymously().catch((error) => {
                    alert(error.message);
                });
            }
        });
    }

    setUid(value) {
        this.uid = value;
    }
    getUid() {
        return this.uid;
    }

    // retrieve the messages from the Backend
    loadMessages(callback) {
        console.log('loadMessages');
        this.messagesRef = Firebase.database().ref('messages');
        this.messagesRef.off();
        const onReceive = (data) => {
            const message = data.val();
            callback({
                _id: data.key,
                text: message.text,
                createdAt: new Date(message.createdAt),
                user: {
                    _id: message.user._id,
                    name: message.user.name,
                },
            });
        };
        this.messagesRef.limitToLast(20).on('child_added', onReceive);
    }

    // Send messages to the backend
    sendMessage(message) {
        console.log('sendMessage: ' + message);
        for(let i=0; i < message.length; i++) {
            this.messagesRef.push({
                text: message[i].text,
                user: message[i].user,
                createdAt: Firebase.database.ServerValue.TIMESTAMP,
            });
        }
    }

    // Close the connection to the Backend
    closeChat() {
        if (this.messagesRef) {
            this.messagesRef.off();
        }
    }
}


export default new Backend();
