import { SEND_MESSAGE,
         GET_ALL_MESSAGES,
         RECEIVE_MESSAGE,
         SET_NAME
       } from '../actions'

export default function chats(state = {messages: [],  name: null,}, action) {
    switch (action.type) {
    case SET_NAME: {
        return {
            ...state,
            name: action.name,
        }
    }
    case GET_ALL_MESSAGES: {
        return {
            ...state,
            messages: [...state.messages, action.payload],
        }
    }
    case SEND_MESSAGE: {
        return {
            ...state,
            message: action.payload,
        }
    }
    case RECEIVE_MESSAGE: {
        return {
            ...state,
            messages: [...state.messages, action.payload.messages],
        }
    }
    }

    return state;
}
