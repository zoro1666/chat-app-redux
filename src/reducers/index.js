import { combineReducers } from 'redux';
import chats  from './chatReducer';

export default combineReducers({
  chats
});
