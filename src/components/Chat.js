import React from 'react';
import { connect } from 'react-redux';

import {
    GiftedChat,
} from 'react-native-gifted-chat';
import Backend from '../Backend'

import { getMessages  }  from '../actions/chatActions';

class Chat extends React.Component {

    render() {
        const { name, messages } = this.props
        return (
            <GiftedChat
              messages={this.props.messages}
              onSend={(message) => {
                  // send to the backend
                  Backend.sendMessage(message);
              }}
              user={{
                  _id: Backend.getUid(),
                  name: name,
              }}
              />
        );
    }
    getMessages() {
        this.props.dispatch(getMessages());
    }
    componentWillMount() {
        console.log(this.props);
        console.log(GiftedChat);

        this.getMessages();

        console.log("messages: " + this.props.messages);

        // Backend.loadMessages((message) => {
        //     // console.log('message: ' + JSON.stringify(message));

        //     this.getMessages(messages);
        //     console.log('messages: ' + this.props.messages);

        //     return {
        //         messages: GiftedChat.append(this.props.messages, message),
        //     }

        //     // this.setState((previousState) => {
        //     //     return {
        //     //         messages: GiftedChat.append(previousState.messages, message),
        //     //     };
        //     // });
        // });
    }
    componentWillUnmount() {
        Backend.closeChat();
    }

}

function mapStateToProps(state) {
    return {
        messages: state.chats.messages,
        name: state.chats.name,
    }
}

export default connect(mapStateToProps)(Chat);
