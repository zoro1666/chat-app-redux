import React from 'react';
import { connect } from 'react-redux';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity
} from 'react-native';

import {
    Actions,
} from 'react-native-router-flux';

import { setName } from '../actions/chatActions'

class Home extends React.Component {
    setName(text) {
        this.props.dispatch(setName(text));
    }
    componentDidMount() {
        console.log(this.props);
    }
    render() {
        return (
            <View>
              <Text style={styles.title}>
                Enter your name :
              </Text>
              <TextInput
                style={styles.nameInput}
                placeholder='Roronoa Zoro'
                onChangeText={(text) => {
                    this.setName(text);
                }}
                value={this.props.name}
                />
                <TouchableOpacity
                  onPress={() => {
                      Actions.chat({
                          name: this.props.name,
                      });
                  } }
                  >
                  <Text style={styles.buttonText}>
                    Next
                  </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

var styles = StyleSheet.create({
    title: {
        marginTop: 20,
        marginLeft: 20,
        fontSize: 20
    },
    nameInput: {
        padding: 5,
        height: 40,
        borderWidth: 1,
        borderColor: 'black',
        margin: 20
    },
    buttonText: {
        fontSize: 20,
        margin: 20
    }
});

function mapStateToProps(state) {
    return {
        name: state.chats.name,
    }
}

export default connect(mapStateToProps)(Home);
