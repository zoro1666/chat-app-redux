import React, { Component } from 'react';
import { Provider } from 'react-redux';

import Home from './components/Home';
import Chat from './components/Chat';
import store from './store';

import {
    Router,
    Scene,
} from 'react-native-router-flux';

import {
    Platform
} from 'react-native';

class App extends Component {
    render() {
        return (
	    <Provider store={store}>
                  <Router>
		    <Scene key='root' style={{paddingTop: Platform.OS === 'android' ? 54 : 64}}>
                      <Scene key='home' component={Home} title='Home' />
                      <Scene key='chat' component={Chat} title='Chat' />
		    </Scene>
                </Router>
	    </Provider>
        );
    }
}

export default App;
