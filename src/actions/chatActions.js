import Backend from '../Backend'

export const SEND_MESSAGE = 'SEND_MESSAGE'
export const GET_ALL_MESSAGES = 'GET_ALL_MESSAGES'
export const RECEIVE_MESSAGE = 'RECEIVE_MESSAGE'
export const SET_NAME = 'SET_NAME'

export function setName(name) {
    return {
        type: SET_NAME,
        name,
    }
}
export function getMessages() {
    var tempMessages = [];
    return function(dispatch) {
        Backend.loadMessages((message) => {
            tempMessages.push(message);
            // dispatch({
            //     type: GET_ALL_MESSAGES,
            //     payload: message,
            // });
        });
        dispatch({
            type: GET_ALL_MESSAGES,
            payload: tempMessages,
        });
    }
}

export function newMessage(payload) {
    return {
        type: RECEIVE_MESSAGE,
        payload,
    }
}

export function sendMessage(message) {
    return {
        type: SEND_MESSAGE,
        payload: {
            message: message,
        }
    }
}
